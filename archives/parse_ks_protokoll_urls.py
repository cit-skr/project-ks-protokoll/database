import os, sys, math

parent_folder_path = os.path.dirname( os.path.abspath(__file__)).split(r'src')[0]
sys.path.append(parent_folder_path)

from src.client.sql_client import Database
import src.client.sql_scripts as scripts

database = Database.get_session()


excel_file_path = parent_folder_path+ "/assets/KS_protokoll_url_20200519.xlsx"

import pandas as pd

ks_protokoll_df = pd.read_excel(excel_file_path)
con = database.connect()
database.execute(con, scripts.set_timezone)
for i in range(len(ks_protokoll_df)):
    row = ks_protokoll_df.iloc[i,:]
    comment = row['Comment'] if type(row['Comment']) is str else '' 
    con = database.connect()
    try:
        database.insert_municipality_info(
            con, 
            int(row['Kommunkod']),
            row['Kommun'],
            int(row['Folkmängd 31 december 2019']),
            row['Huvudgrupp kod'],
            row['Huvudgrupp namn'],
            int(row['Kommungrupp kod']),
            row['Kommungrupp 2017 namn'],
            int(row['Länskod']),
            row['Län namn'],
            row['Landsting/region namn'],
            comment,
            row['Url']
        )

    except BaseException as e:
        break
        sys.exit(1)

