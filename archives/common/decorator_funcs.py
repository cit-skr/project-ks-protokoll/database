import functools
import time, os, sys
parent_folder_path = os.path.dirname( os.path.abspath(__file__)).split(r'src')[0]
sys.path.append(parent_folder_path)

# from src.common.logging_service import getLogger
# logger = getLogger(os.path.basename(__file__).split('.')[0])

def slow_down(_func=None, *, rate=1):
    """Sleep given amount of seconds before calling the function"""
    def decorator_slow_down(func):
        @functools.wraps(func)
        def wrapper_slow_down(*args, **kwargs):
            time.sleep(rate)
            return func(*args, **kwargs)
        return wrapper_slow_down

    if _func is None:
        return decorator_slow_down
    else:
        return decorator_slow_down(_func)

def timer(func):
    """Print the runtime of the decorated function"""
    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        start_time = time.perf_counter()    # 1
        value = func(*args, **kwargs)
        end_time = time.perf_counter()      # 2
        run_time = round((end_time - start_time),4)    # 3
        print("Finished " + str(func.__name__) + " in "+ str(run_time) + " secs")
        return value
    return wrapper_timer

def try_except(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        try: 
            return func(*args, **kwargs)
        except BaseException as e:
            print("from>>"+str(func.__name__),e)
            return False
    return wrapper

@timer
@try_except
@slow_down(rate=0.2)
def test():
    print("!!")


if __name__=='__main__':
    test()