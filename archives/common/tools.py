import os, threading, sys, time, errno, contextlib, json
import numpy as np
from itertools import groupby
from datetime import datetime
from dateutil.tz import gettz
from collections import namedtuple, deque

parent_folder_path = os.path.dirname( os.path.abspath(__file__)).split(r'src')[0]
sys.path.append(parent_folder_path)

def json2obj(data): return json.loads(str(data).replace("\'","\""), object_hook=lambda d: namedtuple("X", d.keys())(*d.values()))

def now():  
    dt = datetime.now(gettz("Europe/Stockholm")).strftime('%Y-%m-%d %H-%M-%S.%d')[:-3]
    return str(dt.replace(' ','_'))

def date_today():  
    dt = datetime.now(gettz("Europe/Stockholm")).strftime('%Y-%m-%d')
    return str(dt.replace(' ','_'))

def get_filepath(dir_ = ".",filterby = ".meta",indx=-1):
    cwd = os.getcwd()
    os.chdir(dir_)
    file_name = [file_name for file_name in list(sorted( os.listdir('.'), key=os.path.getmtime)) if filterby in file_name][indx]
    os.chdir(cwd)
    return dir_ + "/" +file_name

def get_folder_size(folder_name):
    " works with in the directory of the script"
    script_dir = os.path.dirname(__file__)
    rel_path = folder_name
    abs_folder_path = os.path.join(script_dir, rel_path)
    k = int(sum([entry.stat().st_size for entry in os.scandir(abs_folder_path)])/(1024*1024))
    return k


def mkdir_p(path, folder_name = None):
    if folder_name:
        abs_folder_path = os.path.join(path, folder_name)
    else:
        abs_folder_path = path
    try:
        os.makedirs(abs_folder_path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(abs_folder_path):
            pass
        else:
            raise


def get_parent_dir(ref = 'tf_app'):
    cwd = os.getcwd()
    dir_l = ['.','..//','..//']
    for d in dir_l:
      os.chdir(d)
      sub_dir = list(next(os.walk('.'))[1])
      if ref in sub_dir:
        ans = os.getcwd()
        os.chdir(cwd)
        return ans

class AttrDict(dict):
  __getattr__ = dict.__getitem__
  __setattr__ = dict.__setitem__

