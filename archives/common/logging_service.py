import os, sys, json, logging, logging.config, time, errno, zmq, socket, traceback, getpass

from datetime import datetime
from logging import FileHandler
from logging.handlers import RotatingFileHandler
from threading import Thread
from queue import Queue
from collections import deque
from dateutil.tz import gettz

parent_folder_path = os.path.dirname( os.path.abspath(__file__)).split(r'src')[0]
sys.path.append(parent_folder_path)

logger_config_path = parent_folder_path+ "/src/common/default_config.json"

from src.common.tools import mkdir_p, AttrDict

class AsyncHandlerMixin(object):
    def __init__(self, *args, **kwargs):
        super(AsyncHandlerMixin, self).__init__(*args, **kwargs)
        self.__queue = Queue()
        self.__thread = Thread(target=self.__loop)
        self.__thread.daemon = True
        self.__thread.start()
        self.start_time = time.time()
        self.call_count = 0

    def emit(self, record):
        rate = round(self.call_count/(time.time()-self.start_time),2)
        if rate<5.0:
            self.__queue.put(record)
            self.call_count+=1
        
    def __loop(self):
        while True:
            time.sleep(0.1)
            record = self.__queue.get()
            try:
                super(AsyncHandlerMixin, self).emit(record)
            except:
                pass


class AsyncRotatingFileHandler(AsyncHandlerMixin, RotatingFileHandler):
    pass


class ZmqHandler(logging.Handler): 
    def __init__(self,logger,port=1234):
        logging.Handler.__init__(self)
        self.logger = logger
        self.hostname = socket.gethostname()
        self.username = getpass.getuser()
        context = zmq.Context()
        self.socket = context.socket(zmq.PUB)
        self.socket.RCVTIMEO = 60*1000
        try:
            self.socket.connect("tcp://127.0.0.1:%s" % port)
        except:
            self.logger.exception("from ZmqService>>init_server")
            port += 1
            pass
        # host = "127.0.0.1"
        # self.logger.info("Serving zmq logger service on local host: "+"tcp://" + str(host) + ":"+str(port))
        self.__queue = deque([],maxlen=500)
        self.__thread = Thread(target=self.__loop)
        self.__thread.daemon = True
        self.__thread.start()
        self.start_time = time.time()
        self.call_count = 0

    def send(self,data,flags=""):
        try:
            self.socket.send_string("%s;%s" % ("logs",json.dumps(data)))
        except zmq.error.ZMQError:
            self.logger.error("Can't send message/Resource temporarily unavailable!")
        except:
            self.logger.exception("from ZmqService>>send")
        
    def emit(self, record):
        thisdate = datetime.now(gettz("Europe/Stockholm"))
        msg = record.msg
        if record.exc_info:
            tb = traceback.format_exception(record.exc_info[0],record.exc_info[1],record.exc_info[2])
            msg += "\n"+''.join(tb)
        source = record.filename+"/"+ record.funcName+":"+str(record.lineno)
        
        data =  {"host": self.hostname,"user":self.username, "date_time":str(thisdate), "logger_name": record.name,"source": source, "level": record.levelname, "message": msg}
        rate = round(self.call_count/(time.time()-self.start_time),2)
        if rate<5.0:
            self.__queue.append(data)
            self.call_count+=1

    def __loop(self):
        while True:
            time.sleep(0.1)
            if len(self.__queue)>0:
                try:                    
                    self.send(self.__queue.popleft())
                except:
                    self.logger.exception("from >> __loop")


def getLogger( logger_name,logfile = "common.log", path=None):
    logger = logging.getLogger(logger_name)
    default_level=logging.INFO

    if os.path.exists(logger_config_path):
        with open(logger_config_path, 'rt') as f:
            config = AttrDict()
            config.update(json.load(f)["logger_config"])
        logging.config.dictConfig(config)
        if config.file_handler:
            file_handler = AsyncRotatingFileHandler(
                logfilepath,
                maxBytes=config.file_handler_config["maxBytes"],
                backupCount=config.file_handler_config["backupCount"]
            )
            logger.addHandler(file_handler)

        if config.zmq_handler:
            zmq_handler = ZmqHandler(logger,port=config.zmq_handler_config["port_internal"])
            logger.addHandler(zmq_handler)

        # if config.sqlite_handler:
        #     file_name = "SensorData_"+str(datetime.date(datetime.now()))+".sqlite"
        #     sqlite_handler = SQLiteHandler(filename=file_name,logger=logger)
        #     logger.addHandler(sqlite_handler)
    else:
        logging.basicConfig(level=default_level)

    if not path:
        path = parent_folder_path+"/local_cache/logs/"
    mkdir_p(path)
    
    logfilepath = os.path.join(path, logfile)

   

    return logger



def test_logger(logger_name="my_logger3"):
    logger = getLogger(logger_name,logfile="data.log")
    st = time.time()
    c = 0
    while True:
        try:
            logger.info("info!!")
            logger.debug("debug!")
            logger.error("error!")
            time.sleep(1)
            c+=1
            rate = round(c/(time.time()-st),2)
            print(rate," calls/sec")

        except KeyboardInterrupt:
            logger.error("exception")
            logger.exception("message:")
            break
        except:
            raise

if __name__ == '__main__':
    try:
        mode = sys.argv[1]
    except:
        mode = "test"

    if mode=="test":
        test_logger(logger_name="test_logger")
    


    

