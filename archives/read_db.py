import os, sys, math

parent_folder_path = os.path.dirname( os.path.abspath(__file__)).split(r'src')[0]
sys.path.append(parent_folder_path)

from src.client.sql_client import Database
import src.client.sql_scripts as scripts

session = Database.get_session()

con = session.connect()

for a in session.get_municipality_info(con):
    print(a.get('name'))
    print([k for k in a.keys()])
    break