
schema_name = "ks_protokoll_schema"
setup_db = """
    CREATE USER sidkas WITH ENCRYPTED PASSWORD 'cit_digi';

    CREATE DATABASE postgres_db
        WITH
        OWNER = sidkas
        ENCODING = 'UTF8'
        LC_COLLATE = 'en_US.utf8'
        LC_CTYPE = 'en_US.utf8'
        TABLESPACE = pg_default
        CONNECTION LIMIT = -1;
"""
create_schema_old = """
    DROP SCHEMA IF EXISTS ks_protokoll_schema CASCADE;
    CREATE SCHEMA ks_protokoll_schema AUTHORIZATION sidkas;
"""

create_schema = """
    DROP SCHEMA IF EXISTS ks_protokoll_schema CASCADE;
    CREATE SCHEMA ks_protokoll_schema;
"""

create_table_file_info = """
    CREATE TABLE IF NOT EXISTS ks_protokoll_schema.file_info (
        file_id SERIAL PRIMARY KEY,
        created_on TIMESTAMP NOT NULL,
        last_usage TIMESTAMP CHECK (last_usage >= created_on),
        last_updated TIMESTAMP CHECK (last_updated >= created_on),
        webpage_url VARCHAR,
        url_text VARCHAR,
        pdf_url VARCHAR UNIQUE,
        relative_path VARCHAR UNIQUE,
        relative_path_text VARCHAR UNIQUE,
        n_requests INTEGER,
        size_in_bytes INTEGER,
        fs_id int REFERENCES ks_protokoll_schema.file_system_info (fs_id),
        municipality_id int REFERENCES ks_protokoll_schema.municipality_info (municipality_id)
    );
"""

create_table_file_system_info = """
    CREATE TABLE IF NOT EXISTS ks_protokoll_schema.file_system_info (
        fs_id SERIAL PRIMARY KEY,
        abs_path VARCHAR UNIQUE NOT NULL,
        free_memory VARCHAR NOT NULL,
        ip_address VARCHAR UNIQUE NOT NULL,
        path_id_rsa VARCHAR UNIQUE NOT NULL
    );
"""


create_table_municipality_info = """
    DROP TYPE IF EXISTS status;
    CREATE TYPE status AS ENUM ('complete', 'error', 'check_this_manually', 'in_progress', 'todo');
    CREATE TABLE IF NOT EXISTS ks_protokoll_schema.municipality_info (
        municipality_id serial PRIMARY KEY,
        created_on TIMESTAMP NOT NULL,
        code INTEGER UNIQUE NOT NULL,
        name VARCHAR UNIQUE NOT NULL,
        population INTEGER NOT NULL,
        main_group_code VARCHAR NOT NULL,
        main_group_name VARCHAR NOT NULL,
        municipality_group_code INTEGER NOT NULL,
        municipality_group_name VARCHAR NOT NULL,
        county_code INTEGER NOT NULL,
        county_name VARCHAR NOT NULL,
        region_name VARCHAR NOT NULL,
        comment VARCHAR default '',
        url VARCHAR NOT NULL,
        urls_to_scrape TEXT [],
        latest_update TIMESTAMP,
        scrape_status status default 'todo',
        n_pdfs INTEGER default 0
    );
"""

create_table_model_info = """
CREATE TABLE IF NOT EXISTS ks_protokoll_schema.model_info (
            model_id serial PRIMARY KEY,
            model_name VARCHAR (20) NOT NULL,
            path VARCHAR UNIQUE NOT NULL,
            training_accuracy NUMERIC (5,4) NOT NULL,
            test_accuracy NUMERIC (5,4) NOT NULL,
            created_on TIMESTAMP NOT NULL,
            last_updated TIMESTAMP CHECK (last_updated >= created_on)
        );

"""

create_view = """
    CREATE VIEW MetaInfo AS
        SELECT count (file_id) as nFiles, max (created_on) as recent_update /*, count(n_requests = 0) as nUnusedFiles */
        FROM file_info
    ;
"""


check_db_exists = "SELECT schema_name FROM information_schema.schemata WHERE schema_name = 'ks_protokoll_schema'"

insert_file_system_info = "INSERT INTO ks_protokoll_schema.file_system_info (abs_path, free_memory, ip_address, path_id_rsa) VALUES (%s, %s, %s, %s);"

check_path_exists = "SELECT recording_id FROM ks_protokoll_schema.file_info WHERE relative_path = '%s'"

insert_municipality_info = """
    set timezone to 'Europe/Paris';
    select now();
    INSERT INTO 
        ks_protokoll_schema.municipality_info 
        (created_on, code, name, population, main_group_code, main_group_name, municipality_group_code, municipality_group_name, county_code, county_name, region_name, comment, url) 
    VALUES 
        (NOW(), %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s );
"""

get_municipality_info = """
    SELECT %s FROM ks_protokoll_schema.municipality_info;
"""

insert_file_info = """
INSERT INTO ks_protokoll_schema.file_info
(created_on,last_usage,last_updated,pdf_url,relative_path,relative_path_text,n_requests,size_in_bytes,fs_id,municipality_id)
VALUES(NOW(),NOW(),NOW(),%s, %s, %s, %s, %s, %s, %s);
"""
get_all_tables = """
SELECT table_name
  FROM information_schema.tables
 WHERE table_schema='ks_protokoll_schema'
   AND table_type='BASE TABLE';
"""

set_timezone = "set timezone to 'Europe/Stockholm';"