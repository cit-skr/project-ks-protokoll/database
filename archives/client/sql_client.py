import asyncpg, asyncio, json, io, os, sys, collections, time, subprocess
import numpy as np
from urllib.parse import urlparse
parent_folder_path = os.path.dirname( os.path.abspath(__file__)).split(r'src')[0]
sys.path.append(parent_folder_path)

import src.client.sql_scripts as scripts
from src.common.decorator_funcs import try_except
from src.common.tools import now


import psycopg2
import psycopg2.extras

class Database:
    local = True
    def __init__(self, url=None):
        self.con = None
        self.cursor = None
        self.connected = False
        self.url = url
    @classmethod
    def get_heroku_db_url(self):
        bashCommand = "heroku config:get DATABASE_URL -a ks-protokoll-parser"
        process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
        stdout, stderr = process.communicate()
        return stdout.decode().split('\n')[0]

    @classmethod
    def get_session(self):
        if not self.local:
            url = self.get_heroku_db_url()
        else:
            print("#############################################################")
            print("######################  local database  ######################")
            print("#############################################################")
            url = 'postgres://sidkas:cit_digi@0.0.0.0:5432/postgres_db' 
            # url = 'postgres://sidkas:cit_digi@localhost:5432/postgres_db' ##Note: <<-- change this

        database = Database(url=url)
        return database
    
    @try_except
    def connect(self):
        self.parsed_url = urlparse(self.url)
        self.con = psycopg2.connect(
            database = self.parsed_url.path[1:],
            user = self.parsed_url.username,
            password = self.parsed_url.password,
            host = self.parsed_url.hostname,
            port = self.parsed_url.port
        )
        self.cursor = self.con.cursor()
        self.connected = False
        return self.con
    
    @try_except
    def get_version(self,con):
        try:
            cur = con.cursor()
            cur.execute('SELECT version()')
            version = cur.fetchone()[0]
            print(version)
        except psycopg2.DatabaseError as e:
            print(f'Error {e}')
            sys.exit(1)
        finally:
            if con:
                con.close()

    def close(self):
        if self.con:
            self.con.commit()
            self.cursor.close()
            self.con.close()

    def create_schema(self,con):
        with con:
            cur = con.cursor()
            if self.local:
                cur.execute(scripts.create_schema_old)
            else:
                cur.execute(scripts.create_schema)
            cur.execute(scripts.create_table_file_system_info)
            cur.execute(scripts.create_table_municipality_info)
            cur.execute(scripts.create_table_file_info)
            cur.execute(scripts.create_table_model_info)
            con.commit()
            return True
    @try_except
    def insert_municipality_info(
        self,con,code,name,population,main_group_code,main_group_name,
        municipality_group_code,municipality_group_name,county_code,
        county_name,region_name,comment,url):
        l = (code,name,population,main_group_code,main_group_name,municipality_group_code,municipality_group_name,county_code,county_name,region_name,comment,url)
        last_row_id = self.insert_one(con, scripts.insert_municipality_info, l)

        return last_row_id

    @try_except
    def get_municipality_info(self,con):
        return self.fetch_one(con,scripts.get_municipality_info % "municipality_id,created_on, code, name, population, main_group_code, main_group_name, municipality_group_code, municipality_group_name, county_code, county_name, region_name, comment, url, scrape_status, n_pdfs, latest_update" )

    @try_except
    def insert_file_info(self,con,url,relative_path,relative_path_text,n_requests,size_in_bytes,fs_id,municipality_id):
        last_row_id = self.insert_one(
            con,
            scripts.insert_file_info,
            (url,relative_path,relative_path_text,n_requests,size_in_bytes,fs_id,municipality_id)
        )
        return last_row_id
    
    @try_except
    def insert_file_system_info(self,con,abs_path, free_memory, ip_address, path_id_rsa):
        last_row_id = self.insert_one(
            con,
            scripts.insert_file_system_info,
            (abs_path, free_memory, ip_address, path_id_rsa)
        )
        return last_row_id

    @try_except
    def get_all_tables(self,con):
        return self.fetch_all(con, scripts.get_all_tables)

    @try_except
    def fetch_all(self,con,query):
        with con:
            cur = con.cursor()
            cur.execute(query)
            rows = cur.fetchall()
            return rows
    @try_except
    def fetch_one(self,con,query):
        with con:
            cur = con.cursor(cursor_factory = psycopg2.extras.DictCursor)
            cur.execute(query)
            while True:
                row = cur.fetchone()
                if row == None:
                    break
                yield row
    @try_except
    def insert_many(self,con,query,data):
        with con:
            cur = con.cursor()
            cur.executemany(query, data)
            last_row_id = cur.fetchone()[0]
            con.commit()
            return last_row_id
    @try_except
    def insert_one(self,con,query,data):
        with con:
            cur = con.cursor()
            cur.execute(query, data)
            # last_row_id = cur.fetchone()[0]
            con.commit()
            # return last_row_id
            return True
    @try_except
    def execute(self,con,query):
        with con:
            cur = con.cursor()
            cur.execute(query)
    @try_except
    def reset(self):
        con = self.connect()
        print(session.get_version(con))
        con = session.connect()
        print(session.get_all_tables(con))
        con = session.connect()
        print(session.create_schema(con))


if __name__=='__main__':
    session = Database.get_session()
    session.reset()

    



