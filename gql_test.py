from sgqlc.endpoint.http import HTTPEndpoint

url = 'http://ec2-13-49-27-116.eu-north-1.compute.amazonaws.com:5000/v1/graphql'
headers = {'Content-Type': 'application/json'}

get_extracted_sentences = """
    query MyQuery {
        ks_protokoll_schema_extracted_sentences(order_by: {usage_count: asc}) {
            sentence_id
            municipality_id
            meeting_date
            file_id
            label_id
            label
            text
            usage_count
        }
    }
"""
# query = 'query { ... }'
# variables = {'varName': 'value'}

endpoint = HTTPEndpoint(url, headers)
data = endpoint(get_extracted_sentences)
print(data)